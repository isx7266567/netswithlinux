#### 1. ip a

	[...]
	$enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    $link/ether 74:d0:2b:c9:f7:7b brd ff:ff:ff:ff:ff:ff
    $inet 192.168.3.15/16 brd 192.168.255.255 scope global dynamic enp3s0
    $   valid_lft 20470sec preferred_lft 20470sec
    $inet6 fe80::76d0:2bff:fec9:f77b/64 scope link 
    $   valid_lft forever preferred_lft forever

	

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"
    ip a f dev eth0
    
    2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 74:d0:2b:c9:f7:7b brd ff:ff:ff:ff:ff:ff

    
Ponte las siguientes ips en tu tarjeta ethernet: 

2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

	ip a a 2.2.2.2/24 dev enp3s0
	ip a a 3.3.3.3/16 dev enp3s0
	ip a a 4.4.4.4/25 dev enp3s0
	
Consulta la tabla de rutas de tu equipo enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
 
	2.2.2.0/24 dev enp3s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp3s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp3s0  proto kernel  scope link  src 4.4.4.4 

Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:


2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132
	
	
	2.2.2.2  
	PING 2.2.2.2 (2.2.2.2) 56(84) bytes of data.
	64 bytes from 2.2.2.2: icmp_seq=1 ttl=64 time=0.022 ms
	64 bytes from 2.2.2.2: icmp_seq=2 ttl=64 time=0.023 ms

	2.2.2.254   Host Unreachable
	2.2.5.2     Network Unrhable
	3.3.3.35    Host Unreachable
	3.3.200.45  Network Unreachable
	4.4.4.8     Host Unreachable
    4.4.4.132   Host Unreachable
	


#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

Conecta una segunda interfaz de red por el puerto usb

Cambiale el nombre a usb0

Modifica la dirección MAC

Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

Observa la tabla de rutas

#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

Lanzar iperf en modo servidor en cada ordenador

Comprueba con netstat en qué puerto escucha

Conectarse desde otro pc como cliente

Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

Encontrar los 3 paquetes del handshake de tcp

Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas

