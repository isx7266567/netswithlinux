# Resumen de órdenes para redes en linux

# Capa de enlace

Ver las interfaces:

```
ip link show
```

Habilitar/deshabilitar una interface:

```
ip link set eth0 down
ip link set eth0 up
```

Cambiar el nombre o la dirección mac

```
ip link set eth0 down
ip link set eth0 address 00:11:22:33:44:55
ip link set eth0 name lan
ip link set eth0 up
```

Conocer las velocidades disponibles de la tarjeta ethernet
y opciones disponibles:


```
ethtool eth0
```

Ver la velocidad del enlace ethernet actual:
```
ethtool eth0 |grep Speed
```

Conocer driver del kernel que se está utilizando:
```
ethtool -i eth0
```

Ver estadísticas, errores y colisiones:
```
ethtool -S eth0
```

## Vlans

Add virtual interface with tagged vlan 101 to eth0:

```
ip link add link eth0 name vlan101 type vlan id 101
ip link set vlan101 up
```

## Bonding

```
ip link set eth1 down
ip link set eth2 down


modprobe -r bonding
modprobe bonding mode=4

# default xmit_hash_policy is 0 (hash with mac adress)
# modprobe bonding mode=4 xmit_hash_policy=2

ip link set eth1 master bond0
ip link set eth2 master bond0

ip link set eth1 up 
ip link set eth2 up

ip link set bond0 up

cat /proc/net/bonding/bond0

``` 

## Test de velocidad con iperf

La herramienta tradicional es **iperf**, para testear capacidades superiores a 1Gbps mejor usar **iperf3**
Un servidor y un cliente se conectan intercambiando datos al máximo de lo que de la conexión.

```
#Server
iperf -s 
#Client
iperf -c SERVER_IP

```
Con la opción -i se modifica el intervalo en el que muestra estadísticas el servidor. Con la opción -p se modifica el puerto por si quieres hacer varios iperf en paralelo en diferentes puertos.

```
#Server
iperf -s -i 2 -p 5002
#Client
iperf -p 5002 -c SERVER_IP

```

Por defecto el test dura 10 segundos. Con la opción -t puedes modificar este tiempo. 
```
#Server
iperf -s -i 2
#Client
iperf -t 30 -c SERVER_IP

```

## Bridges

Para crear un bridge:
```
brctl addif br0 usb0
```


Para asignar o quitar interfaces al bridge:
```
brctl addif br0 eth0
brctl addif br0 usb0
brctl addif br0 usb1
brctl delif br0 usb1
```

Para ver las interfaces asociadas al bridge y la tabla de puertos y 
direcciones mac:
```
brctl show
brctl show br0
brctl showmacs br0
```



## Captura de tramas

**tcpdump**: herramienta clásica para hacer capturas
```
tcpdump -w 08232010.pcap -i eth0
```

**tshark**: wireshark en línea de comandos, puedes aplicar filtros de captura al final del comando.
Filtros de captura mirar [https://wiki.wireshark.org/CaptureFilters]

* **\[ -i \]** Interface de entrada
* **\[ -w \]** Escribe en fichero
* **\[ -r \]** Leer entrada del fichero
* **\[ -c \]**  Si deseas limitar el número de paquetes a capturar
* **\[ -a duration:X\]** limitar los segundos de la captura
* **\[ -Y \]** filtros de procesado de wireshark

Ejemplos:

```
tshark -i enp3s0 -w /tmp/out.pcap -c 10
tshark -i enp3s0 -w /tmp/out.pcap -a duration:4
tshark -r /tmp/out.pcap -w /tmp/web.pcap -Y "http"

tshark -i enp3s0 -w /tmp/ping.pcap "host 8.8.8.8"
tshark -i enp3s0 -w /tmp/web.pcap "port 80"
```

Para mostrar por pantalla los paquetes podemos verlos de forma resumida o extendida (-x)
```
tshark -r /tmp/web.pcap
tshark -r /tmp/web.pcap -x 
tshark -r /tmp/web.pcap -x -Y "frame.number==10"
```

# Capa de red

Ver direcciones ip con la versión abreviada de **ip address show**:
```
ip a

```

Ver estadísticas de paquetes enviados, recibidos, errors, dropped...
```
ip -statistics a
```

## Ips fijas:

Eliminar todas las direcciones ips asociadas a una interfaz:

```
ip a f dev eth0
```

Asociar una ip a una interfaz (no olvidar la máscara):
```
ip a a 192.168.100.10/24 dev eth0
```

Se pueden asociar más de una ip a una interfaz y eliminar una en concreto:
```
ip a a 192.168.100.10/24 dev eth0
ip a a 192.168.200.11/27 dev eth0

ip a d 192.168.200.11/27 dev eth0

```

## Ips dinámicas:

Liberar la ip actual:
```
dhclient -r
```

Esto debería haber liberado la ip actual y el daemon debería haber finalizado. En caso de que no podamos pedir una nueva ip no nos queda otra más que matar ese proceso con:

```
killall dhclient
```

Para pedir una nueva ip en cualquier interface:
```
dhclient
```

Y en una interface en concreto:
```
dhclient eth0
```

Si quieres ver más detalle de la concesión de ip:
```
dhclient -v eth0
dhclient -v -lf /tmp/eth0.lease
cat /tmp/eth0.lease
```

## Tabla ARP

La orden tradicional arp ha quedado centralizada en la utilidad ip con la opción **ip neigh** 

Para ve la tabla de arp actual:

```
ip neigh
```

Borrar tabla de arp:
```
ip neigh flush all
```

Añadir entrada arp permanente:
```
ip neigh add 192.168.100.1 lladdr 00:11:22:33:44:55 dev enp3s0
```

Ver sólo las entradas reachable:
```
ip neigh show nud reachable
```

## Routing

Ver las rutas con **ip route show**:
```
ip r 
```

Para eliminar todas las rutas "ip route flush":
```
ip r f all
```

Al añadir una dirección ip a una interfaz, se añade una ruta directa
que informa que para ir a la red de esa dirección ip se va directamente
a través de la interfaz sin necesidad de pasar por ningún router:

```
$ ip r f all
$ ip a a 172.16.0.10/16 dev eth0
$ ip r s 
172.16.0.10/16  dev eth0 [...]
```

Añadir puerta de enlace por defecto:
```
ip r a default via 192.168.1.1
```

Eliminar puerta de enlace por defecto:
```
ip r d default
```

Añadir ruta estática:
```
ip r a 192.168.200.0/24 via 192.168.100.1
```

Activar bit de forwarding para que trabaje como router:
```
echo 1 > /proc/sys/net/ipv4/ip_forward
```

## Resolución de nombres DNS

Se consulta la resolución en el fichero **/etc/hosts**, que contiene
una línea para el nombre localhost, se pueden añadir líneas para
resolver nombres sin utilizar un servidor dns:

```
$ cat /etc/hosts
127.0.0.1		localhost.localdomain localhost
```

Para resolver con servidores dns linux consulta el contenido del fichero /etc/resolv.conf
```
$ cat /etc/resolv.conf 
[...]
nameserver 192.168.1.1
```

Se consulta la línea que empieza con nameserver seguida de la ip del servidor dns que queremos usar.
Si al renovar la ip dinámica el servidor dhcp ofrece un servidor dns se sobreescribe este fichero

Para forzar la resolución de nombres a través de un servidor dns de forma manual:
```
echo "nameserver 8.8.8.8" > /etc/resolv.conf
```

## Enmascaramiento de ips

Enmascarar todo el tráfico que sale hacia internet por eth0

```
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
```

Consultar iptables:
```
iptables-save
iptables -t filter -L
iptables -t nat -L
```

## Pruebas de conectividad

Comprabar respuestas protocolo ARP:

```
arping 192.168.100.1
```

Hacer ping indefinidamente (para terminar ctrl + C)

```
ping 8.8.8.8
```


Limitar número de pings

```
ping -c 2 8.8.8.8
```

**fping**: Hacer pings a varios equipos de una red:

```
fping -g 192.168.100.100 192.168.100.110 -a -q
fping -g 192.168.100.0/24 -a -q

```

**nmap** 

Sondeo ping:
```
nmap -sP 192.168.100.0/24
```
Sondeo de puerto 22

## Traceroute y geolocalizar ips

**traceroute**: Herramienta histórica para descubrir rutas

```
traceroute 8.8.8.8
```

**mtr**: Si quieres más información, estadísticas y detalle de geolocalización de ips

```
dnf -y install mtr GeoIP GeoIP-GeoLite-data GeoIP-GeoLite-data-extra
mtr -n --report www.gencat.cat
geoiplookup 72.52.92.222

```

# Capa de transporte

## nmap y sondeo de puertos

Sondeo de puertos TCP típicos:
```
nmap -sS 192.168.100.1
```

Sondeo de un puerto concreto
```
nmap -sS -p 80 192.168.100.1
```

Sondeo de rango de puertos
```
nmap -sS -p 8080-8090 192.168.100.1
```

Sondeo de puertos aunque el ping no conteste, añadiendo la opción -PN
```
nmap -sS -PN -p 80 192.168.100.1
```

Sondeo de puerto UDP:
```
nmap -sU -p 53 192.168.100.1
```

## netstat

número de puertos tcp y udp escuchando con el programa asociado:

```
netstat -utlnp
```

## probar puertos con netcat

