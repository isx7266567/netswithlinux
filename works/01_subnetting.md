

Isx7266567 
Tito Arroyo Corozo
	#http://jodies.de/ipcalc?host=174.187.55.6&mask1=23&mask2=
# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).


IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 |
174.187.55.6/23 | 174.187.54.0 | 174.187.55.255 | 510 |
10.0.25.253/18 | 10.0.192.0|10.0.255.255 | 16382 |
209.165.201.30/27 |209.165.201.208|209.165.201.255 | 128 |


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. 
Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. 
Indica la màscara de xarxa calculada tant en format decimal com en binari.

 Justificacion
   2⁹ = 512  ya que convinacion inferior no nos permitiria los host que necesitmos
   nos que dan 7 bits para para las subredes ya que 2⁷ es 128
    
 asi que sumamos a las mascara 7 bits para ser la c combinaciones de subredes 

11111111.11111111.11111110.00000000/23  = 255.255.254.0 


##### 3. Donada la xarxa 10.192.0.0
a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. 
Indica-la tant en format decimal com en binari. Justificacion

2^11 = 2040 son lo 11 bits que necesitamos para  1500 host el resto lo tomaremos subredes
asumiremos que es un /16 asi que no quedaria algo asi 
11111111.11111111.11111000.00000000 = /21 = 255.255.248.0
 

b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?
11111111.11000000.00000000.000000000/10
establecimos que si para 1500 dispositivos  requeririmos 2^11 bits lo restante
11111111.11111111.11111000.00000000
hasta el /10  que seria son 11 bits 2^11


c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), 
calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. 
Indica-la tant en format decimal com en binari.
a) 11111111.11111111.11111000.00000000 = /21 = 255.255.248.0
necesitamos 2 bits para hacer las subredes nuevas 2²=4 asi que pasamos de un /21 a un /23 y nos quedan 
9 bits para host 2⁹ son 512 
11111111.11111111.11111110.00000000 =23 = 255.255.254



##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius
a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. 
Indica-la tant en format decimal com en binari.

 necesitamos 12 bits para los 2^12=4096
          11111111.11111111.11110000.00000000/20 = 255.255.240.0


b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. 
Indica-la tant en format decimal com en binari.
              
          11111111.11111111.11110000.00000000/20
          
   para hacer 5 subredes nuevas necesitaramos 3bits = 2³ y pasariamos de un /20 a una /23 quedandonos 9 bits 
   para que host que 2⁹ quedandonos insuficiente 510 host

c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, 
quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? 
Indica la fórmula que fas anar per calcular aquesta dada.

  lo maximo que de host que permite son 512 = 2⁹

d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a),
 podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? 
Indica els càlculs que necessites fer per raonar la teva resposta.

mascara apartado (a) 11111111.11111111.11110000.00000000 = 20 
 
 si podemos solo usamos 1 bit para las 2 subxarxes  las mascara nos quedaria 
11111111.11111111.11111000.00000000 / 21
y nos quedarian 11bits para host 2^11 = 2048 que es superior 1625
 





5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.
 11111111.11110000.00000000.00000000/12
a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?
 3 bit 2³ y pasaria a hacer un /15 

b) Dóna la nova MX, tant en format decimal com en binari.
11111111.11111110.00000000.00000000/15  =255.254.0.0

c) Per cada subxarxa nova que has de crear, indica

subred 1  11111111.11111110.00000000.00000000
subred 2  11111111.11111110.01000000.00000000
subred 3  11111111.11111110.10000000.00000000
subred 4  11111111.11111110.11000000.00000000
subred 5  11111111.11111111.00000000.00000000
subred 6  11111111.11111111.01000000.00000000


i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 |172.16.0.0/15 | 172.17.255.255 | 	131070 
Xarxa 2 |172.18.0.0/15 | 172.19.255.255 | 	131070 
Xarxa 3 |172.20.0.0/15 | 172.21.255.255 | 	131070 
Xarxa 4 |172.22.0.0/15 | 172.23.255.255 | 	131070 
Xarxa 5 |172.24.0.0/15 | 172.25.255.255 | 	131070 
Xarxa 6 |172.26.0.0/15 | 172.27.255.255 | 	131070 



d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? 
Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

 2^n >= 6
 donde n es el numero de subredes que necesitamos
 en este caso 2³ que son 8

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

2^n  
n es el numero de host
2^17 =131072


##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

192.168.1.0/26
11111111.11111111.11111111.11000000

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

3 bits ya que  2³=8

11111111.11111111.11111111.11111000 = /29

b) Dóna la nova MX, tant en format decimal com en binari.

11111111.11111111.11111111.11111000 = 255.255.255.244

c) Per cada subxarxa nova que has de crear, indica



i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 |192.168.1.0/26  	|	192.168.1.63/26  |		62
Xarxa 2 |192.168.1.64/26  	|192.168.1.127/26  	 |		62
Xarxa 3 |192.168.1.128/26  	|192.168.1.191/26  	 |		62
Xarxa 4 |192.168.1.192/26  	|192.168.1.255/26  	 |		62


d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? 
Dóna'n la fórmula que has utilitzat per respondre a la pregunta.

2^n  >= n
si, usando 2 bits mas 

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? 
Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

en el A) 2⁶ =62
en el B) 2³ =8

